import { NuxtAxiosInstance } from "@nuxtjs/axios"
import { Auth } from "@nuxtjs/auth"

interface User {
}

// https://qiita.com/hareku/items/b3e7c5427d0f4cb822a7
export interface Context {
  $axios: NuxtAxiosInstance
}

// https://qiita.com/Aruneko/items/552fcd3ae5da4eb1b218
declare module 'vue/types/vue' {
  interface Vue {
    $auth: Auth
    user: User
  }
}
