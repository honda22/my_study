import Vue from "vue"

export type DataType = {
  helloWorld: string
}

export default Vue.extend({
  data(): DataType {
    return {
      helloWorld: 'ようこそアプリケーションへ'
    }
  }
})
