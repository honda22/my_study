# MY_STUDY
## 環境
- Ruby (Ruby on Rails)
- Vue.js (nuxt.js)
- Typescript
- Docker
- Mysql

### backend rails

```
フォルダ構成
├── README.md
├── frontend
├── backend
└── docker-compose.yml
```

1. bundle init
Gemfileに gem 'rails', '6.0.3'
2. backendディレクトリにDockerfile作成
3. docker-compose.ymlを作成
4. docker-compose run backend rails new . --api --force --database=mysql --skip-bundle
5. config/database.ymlを変更

```
default: &default
  adapter: mysql2
  encoding: utf8mb4
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  username: root
  password:

development:
  <<: *default
  host: db
  database: app_development
test:
  <<: *default
  host: <%= ENV['DB_HOST'] || 'db' %>
  database: app_test
```

6. docker-compose build
7. docker-compose up -d
8. docker-compose exec backend bin/rails db:create

### frontend nuxt.js

1. frontendディレクトリにDockerfile作成
2. docker-compose.yml に frontend の記述を追加
3. docker-compose build
4. docker-compose run frontend npx create-nuxt-app

```
docker-compose run frontend npx create-nuxt-app
Creating my_study_frontend_run ... done

create-nuxt-app v3.4.0
✨  Generating Nuxt.js project in .
? Project name: app
? Programming language: TypeScript
? Package manager: Npm
? UI framework: Vuetify.js
? Nuxt.js modules: Axios
? Linting tools: ESLint, Prettier, StyleLint
? Testing framework: Jest
? Rendering mode: Universal (SSR / SSG)
? Deployment target: Server (Node.js hosting)
? Development tools: (Press <space> to select, <a> to toggle all, <i> t
o invert selection)
? Continuous integration: None
? Version control system: None
```
## アプリの実装

### ログイン機能の実装

